# Description of this Program
# Author : Sarunya Nawwapornnimit
# Since : 2021-23-16
# Program Name : Read & Write File
# Program Language : Python
# Program Purpose : Write a small program


def read_index(str):                                                                # reed file function
    filename = 'text.txt'                                                           # value of text.txt
    f_object = open(filename,'r')                                                   # give f_object to open text.txt and turn on read function
    print(f_object.read())                                                          # print read function
    f_object.close()                                                                # close file
    
def delete_index(str):                                                              # delete file function
    filename = 'text.txt'                                                           # value of text.txt
    f_object = open(filename,'w')                                                   # value of text.txt use open file
    f_object.write("")                                                              # write the blank into the file = delete file
    f_object.close                                                                  # close file

def dot_check(dotcheck) :                                                           # float check function
    try:
        float(dotcheck)                                                             # if data to insert in this file is float do continue
        return True                                                                 
    except ValueError:                                                              # if data to insert in this file is not float do remind : ValueError
        return False
    
def add_index(str):                                                                 # add file function
    num_data = int(input("How many do you add to the file? : "))                    # input the number of data to add the file give the valuename is num_data
    index = []                                                                      # give the valuename : index is blank list and collect the data
    count_index = 0
    for indx in range(num_data):                                                    # loop for to insert the data observe by number of data       
        insert_data = input("insert your data : ")
        if insert_data.isnumeric():                                                 # if insert data is number keeps user insert next data
            index.append(insert_data)                                       
            count_index += 1                                                                                               
        else :
            if dot_check(insert_data) is True:                                      # if do dot_check function is true keeps user insert next data
                index.append(insert_data)
                count_index += 1
            else :
                count_index += 1                                                    
                print("DataError \n Please insert the integer or float number")
                print(check_add(str))                                               # do check_add function
                
    if len(index) == count_index:                                                   # if num of insert data == postion of list
        filename = 'text.txt'                                                       # value of text.txt
        f_object = open(filename,'w')                                               # give f_object to open text.txt and turn on read function
        for N_data in index:                                                        # loop for to write data observe by insert the data
            f_object.write(N_data + '\n') 
        f_object.close()                                                            # close file
        
def check_add(str):                                                                 # check_add function
    print("1 = restart the program")                                                
    print("2 = end the program")
    reprogrammee = str(input("\n Do you add data again? : "))                       # value name : reprogrammee get value from user to add data OR end the porogram
    if reprogrammee == '1':                                                         # if user select 1 : program will restart
        print(add_index(str))                                                       # do add_index function
    elif reprogrammee == '2':                                                       # if user select 2 : program will end
        pass
    else:
        print("Please select 1 OR 2 again!!")                                       # # else out of above
        print(check_add(str))                                                       # do check_add function
    
def accept_index(str):                                                              # accept file function
     select1 = str(input("Are you accept this data? y/n : "))                       # input Yes/No to accept data use value name : select1
     if select1 == 'y':                                                             # if select 'y' do pass function
         pass
     elif select1 == 'n':                                                           # but select 'n' do add file function
         print(add_index(str))
         
def replace_index(str):                                                             # replace file function                  
    num_data = int(input("How many do you replace to the file? : "))                # input the number of data to replace the file give the valuename is num_data
    index = []                                                                      # give the valuename : index is blank list and collect the data
    count_index = 0
    for indx in range(num_data):                                                    # loop for to insert the data observe by number of data  
        insert_data = input("insert your data : ")
        if insert_data.isnumeric():                                                 # if insert data is number keeps user insert next data
            index.append(insert_data)
            count_index += 1
        else :
            if dot_check(insert_data) is True:                                      # if do dot_check function is true keeps user insert next data
                index.append(insert_data)
                count_index += 1                                                    
            else :
                count_index += 1
                print("DataError \n Please insert the integer or float number")
                print(check_replace(str))                                           # do check_add function
    
    if len(index) == count_index:                                                   # if num of insert data == postion of list
        insert_fname = input("Please write your file name : ")                      # give the user insert filename to replace the file
        f_object = open(insert_fname+".txt",'w')                                    # use the filename insert by user to open the write function
        for N_data in index:                                                        # loop for to write data observe by insert the data
            f_object.write(N_data + '\n') 
            f_object.close()                                                        # close file

def check_replace(str):                                                             # check_replace function
    print("1 = restart the program")
    print("2 = end the program")
    reprogrammee = str(input("\n Do you replace data again? : "))                   # value name : reprogrammee get value from user to replace data OR end the porogram
    if reprogrammee == '1':                                                           # if user select 1 : program will restart
        print(replace_index(str))                                                   # do replace_index function
    elif reprogrammee == '2':                                                         # if user select 2 : program will end
        pass
    else:                                                                           # else out of above
        print("Please select 1 OR 2 again!!")
        print(check_replace(str))                                                   # do check_replace function
    
def choose_func(str):                                                               # choose_func function                                       
    print("1 Delete")                                                   
    print("2 Add")
    print("3 Accept")
    print("4 Replace")
    print("5 Read")
    choose1 = str(input("Select the function : "))                                  # give user to select the function value name : choose1
    if choose1 == '1':                                                              # select number 1
        print(delete_index(str))                                                    # do delete file function
    elif choose1 == '2':                                                            # select number 2
        print(add_index(str))                                                       # do add file function
    elif choose1 == '3':                                                            # select number 3
        print(accept_index(str))                                                    # do accept file function
    elif choose1 == '4':                                                            # select number 4
        print(replace_index(str))                                                   # do replace file function
    elif choose1 == '5':                                                            # select number 5
        print(read_index(str))                                                      # do read file function
    else:
        print("Please select the function again")                                   # out of the number above remind user to select again
        print(check_choose(str))                                                    # do check_choose function
        
def check_choose(str):                                                              # check_choose function
    print("1 = restart the program")    
    print("2 = end the program")
    reprogrammee = str(input("\n Do you restart this progeam again? : "))           # value name : reprogrammee get value from user to restart OR end the porogram
    if reprogrammee == '1':                                                         # if user select 1 : program will restart
        print(choose_func(str))                                                     # do choose_func function
    elif reprogrammee == '2':                                                       # if user select 2 : program will end
        pass                            
    else:                                                                           # else out of above
        print("Please select 1 OR 2 again!!")                                                                          
        print(check_choose(str))                                                    # do check_choose function

print(choose_func(str))                                                             # run program